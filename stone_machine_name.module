<?php

/**
 * Implements hook_menu().
 */
function stone_machine_name_menu() {
  // Make it possible to get to each entity via it's machine name.
  $items['stone-machine-name/%'] = array(
    'page callback' => 'stone_machine_name_redirect',
    'page arguments' => array(1),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function stone_machine_name_permission() {
  return array(
    'allocate machine name' => array(
      'title'       => t( 'Allocate machine name' ) ,
      'description' => t( 'Allow allocation of a machine name to a node' ) ,
    ) ,
  );
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function stone_machine_name_form_node_form_alter(&$form, &$form_state) {
  if (!user_access('allocate machine name')) {
    return;
  }

  $form['stone_machine_name'] = array(
    '#type' => 'fieldset',
    '#group' => 'additional_settings',
    '#title' => 'Machine name',
  );

  if ($machine_names = stone_machine_name_list()) {
    $options = array('<None>');

    foreach ($machine_names as $machine_name => $info) {
      $options[$machine_name] = $info['description'];

      if (list($entity_type, $entity_id) = stone_machine_name_lookup($machine_name)) {
        $options[$machine_name] .= " ($entity_type:$entity_id)";
      }
    }

    $form['stone_machine_name']['stone_machine_name'] = array(
      '#type' => 'select',
      '#title' => 'Machine name',
      '#options' => $options,
    );

    if (isset($form_state['node']->nid) && $machine_name = stone_machine_name_lookup_by_entity($form_state['node']->nid)) {
      $form['stone_machine_name']['stone_machine_name']['#default_value'] = $machine_name;
    }

    $form['#validate'][] = 'stone_machine_name_node_form_validate';
    $form['actions']['submit']['#submit'][] = 'stone_machine_name_node_form_submit';
  }
  else {
    $form['stone_machine_name']['machine_name'] = array(
      '#type' => 'markup',
      '#markup' => 'No machine names have been declared.',
    );
  }
}

/**
 * Validate handler for the node form.
 */
function stone_machine_name_node_form_validate($form, &$form_state) {
  if (isset($form_state['values']['stone_machine_name']) && $machine_name = $form_state['values']['stone_machine_name']) {
    // If this machine name is being used elsewhere then throw an error.
    list(, $entity_id) = stone_machine_name_lookup($machine_name);

    if ($entity_id && (!isset($form_state['node']->nid) || $form_state['node']->nid != $entity_id)) {
      form_set_error('', t('The machine name %machine-name is already being used !here.', array(
        '%machine-name' => $machine_name,
        '!here' => l('here', 'node/' . $entity_id),
      )));
    }
  }
}

/**
 * Submit handler for the node form.
 */
function stone_machine_name_node_form_submit($form, &$form_state) {
  if (isset($form_state['values']['stone_machine_name'])) {
    if ($machine_name = $form_state['values']['stone_machine_name']) {
      $fields = array(
        'entity_type' => 'node',
        'entity_id' => $form_state['nid'],
        'machine_name' => $machine_name,
      );
      db_merge('stone_machine_name')->key($fields)->fields($fields)->execute();
    }
    else {
      // Remove occurrences of this entity type/ID from the machine name table.
      db_delete('stone_machine_name')
        ->condition('entity_type', 'node')
        ->condition('entity_id', $form_state['nid'])
        ->execute();
    }
  }
}

/**
 * Implements hook_node_delete().
 */
function stone_machine_name_node_delete($node) {
  db_delete('stone_machine_name')
    ->condition('entity_type', 'node')
    ->condition('entity_id', $node->nid)
    ->execute();
}

/**
 * Get a list of available machine names.
 */
function stone_machine_name_list() {
  $machine_names = array();

  if ($cache = cache_get('stone_machine_name_list')) {
    $machine_names = $cache->data;
  }
  else {
    $machine_names = module_invoke_all('stone_machine_name');
    drupal_alter('stone_machine_name', $machine_names);
    cache_set('stone_machine_name_list', $machine_names);
  }

  return $machine_names;
}

/**
 * Lookup the entity type and ID that is currently using the given machine name.
 */
function stone_machine_name_lookup($machine_name) {
  $usage = db_select('stone_machine_name', 'mn')
    ->fields('mn')
    ->condition('machine_name', $machine_name)
    ->execute()
    ->fetch();

  if ($usage) {
    return array($usage->entity_type, $usage->entity_id);
  }
}

/**
 * Lookup a machine name given an entity ID/type.
 */
function stone_machine_name_lookup_by_entity($entity_id, $entity_type = 'node') {
  return db_select('stone_machine_name', 'mn')
    ->fields('mn', array('machine_name'))
    ->condition('entity_type', $entity_type)
    ->condition('entity_id', $entity_id)
    ->execute()
    ->fetchField();
}

/**
 * Lookup the URI of the entity using the given machine name.
 */
function stone_machine_name_lookup_uri($machine_name) {
  if (list($entity_type, $entity_id) = stone_machine_name_lookup($machine_name)) {
    $entities = entity_load($entity_type, array($entity_id));
    return entity_uri($entity_type, reset($entities));
  }
}

/**
 * Redirect to the entity with the given machine name.
 */
function stone_machine_name_redirect($machine_name) {
  if ($uri = stone_machine_name_lookup_uri($machine_name)) {
    drupal_goto($uri['path']);
  }

  drupal_not_found();
}
